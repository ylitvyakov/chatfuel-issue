import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';
import { User } from '../user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private userService: UserService
  ) {}

  protected loading: boolean;
  protected saving: boolean;

  @Input() user: User;

  ngOnInit() {
    this.loading = true;

    this.getUser();
  }

  getUser():void {
    const id = this.route.snapshot.paramMap.get('id');

    this.userService.getUser(id)
      .subscribe(user => {
        this.user = user;
        this.loading = false;
      });
  }

  save():void {
    this.saving = true;

    this.userService.updateUser(this.user)
      .subscribe(() => {
        this.saving = false;
      });
  }
}
