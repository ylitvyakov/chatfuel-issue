import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { User } from './user';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class UserService {
  constructor(
    private http: HttpClient
  ) {}

  private URL = 'api/users';

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.URL);
  }

  getUser(id: string): Observable<User> {
    return this.http.get<User>(`${this.URL}/${id}`);
  }

  updateUser(user: User): Observable<any> {
    return this.http.put(this.URL, user, httpOptions);
  }

  fetchUsers(skip: number, limit: number): Observable<User[]> {
    return this.http.get<User[]>(this.URL, {
      params: new HttpParams()
        .append('skip', skip.toString())
        .append('limit', limit.toString())
    });
  }
}
