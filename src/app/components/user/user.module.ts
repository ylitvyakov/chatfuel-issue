import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { UserService } from './user.service';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { VirtualScrollModule } from 'angular2-virtual-scroll';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    VirtualScrollModule,
  ],
  declarations: [
    UsersComponent,
    UserComponent,
  ],
  exports: [
    UsersComponent,
    UserComponent
  ],
  providers: [
    UserService
  ]
})
export class UserModule { }
