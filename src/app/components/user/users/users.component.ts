import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { ChangeEvent } from 'angular2-virtual-scroll';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  constructor(
    private userService: UserService
  ) {}

  @Input()
  users: User[];

  protected loading: boolean;

  ngOnInit() {
    this.loading = true;

    this.getUsers();
  }

  getUsers():void {
    this.userService.getUsers()
      .subscribe(users => {
        this.users = users;
        this.loading = false;
      });
  }

  fetch(event: ChangeEvent):void {
    if (event.end !== this.users.length) return;

    this.loading = true;

    this.userService.fetchUsers(this.users.length, 10)
      .subscribe(users => {
        this.users = this.users.concat(users);
        console.log(this.users);
        this.loading = false;
      })
  }
}
