import { InMemoryDbService } from "angular-in-memory-web-api";

export class inMemoryDataService implements InMemoryDbService {
  createDb() {
    const users = [{
      id: '1',
      name: 'ABC News',
      avatarUrl: '/assets/users/abc.png'
    }, {
      id: '2',
      name: 'Adidas',
      avatarUrl: '/assets/users/adidas.png'
    }, {
      id: '3',
      name: 'British Airways',
      avatarUrl: '/assets/users/britishairways.png'
    }, {
      id: '4',
      name: 'BuzzFeed',
      avatarUrl: '/assets/users/buzz.png'
    }, {
      id: '5',
      name: 'CNBC',
      avatarUrl: '/assets/users/cnbc.png'
    }, {
      id: '6',
      name: 'Complex',
      avatarUrl: '/assets/users/complex.png'
    }, {
      id: '7',
      name: 'Goal.com',
      avatarUrl: '/assets/users/goal.png'
    }, {
      id: '8',
      name: 'JWOWW',
      avatarUrl: '/assets/users/jwoww.png'
    }, {
      id: '9',
      name: 'Lowe\'s Home Improvement',
      avatarUrl: '/assets/users/lowes.png'
    }, {
      id: '10',
      name: 'TechCrunch',
      avatarUrl: '/assets/users/tc.png'
    }, {
      id: '11',
      name: 'ABC News',
      avatarUrl: '/assets/users/abc.png'
    }, {
      id: '12',
      name: 'Adidas',
      avatarUrl: '/assets/users/adidas.png'
    }, {
      id: '13',
      name: 'British Airways',
      avatarUrl: '/assets/users/britishairways.png'
    }, {
      id: '14',
      name: 'BuzzFeed',
      avatarUrl: '/assets/users/buzz.png'
    }, {
      id: '15',
      name: 'CNBC',
      avatarUrl: '/assets/users/cnbc.png'
    }, {
      id: '16',
      name: 'Complex',
      avatarUrl: '/assets/users/complex.png'
    }, {
      id: '17',
      name: 'Goal.com',
      avatarUrl: '/assets/users/goal.png'
    }, {
      id: '18',
      name: 'JWOWW',
      avatarUrl: '/assets/users/jwoww.png'
    }, {
      id: '19',
      name: 'Lowe\'s Home Improvement',
      avatarUrl: '/assets/users/lowes.png'
    }, {
      id: '20',
      name: 'TechCrunch',
      avatarUrl: '/assets/users/tc.png'
    }, {
      id: '21',
      name: 'ABC News',
      avatarUrl: '/assets/users/abc.png'
    }, {
      id: '22',
      name: 'Adidas',
      avatarUrl: '/assets/users/adidas.png'
    }, {
      id: '23',
      name: 'British Airways',
      avatarUrl: '/assets/users/britishairways.png'
    }, {
      id: '24',
      name: 'BuzzFeed',
      avatarUrl: '/assets/users/buzz.png'
    }, {
      id: '25',
      name: 'CNBC',
      avatarUrl: '/assets/users/cnbc.png'
    }, {
      id: '26',
      name: 'Complex',
      avatarUrl: '/assets/users/complex.png'
    }, {
      id: '27',
      name: 'Goal.com',
      avatarUrl: '/assets/users/goal.png'
    }, {
      id: '28',
      name: 'JWOWW',
      avatarUrl: '/assets/users/jwoww.png'
    }, {
      id: '29',
      name: 'Lowe\'s Home Improvement',
      avatarUrl: '/assets/users/lowes.png'
    }, {
      id: '30',
      name: 'TechCrunch',
      avatarUrl: '/assets/users/tc.png'
    }, {
      id: '31',
      name: 'ABC News',
      avatarUrl: '/assets/users/abc.png'
    }, {
      id: '32',
      name: 'Adidas',
      avatarUrl: '/assets/users/adidas.png'
    }, {
      id: '33',
      name: 'British Airways',
      avatarUrl: '/assets/users/britishairways.png'
    }, {
      id: '34',
      name: 'BuzzFeed',
      avatarUrl: '/assets/users/buzz.png'
    }, {
      id: '35',
      name: 'CNBC',
      avatarUrl: '/assets/users/cnbc.png'
    }, {
      id: '36',
      name: 'Complex',
      avatarUrl: '/assets/users/complex.png'
    }, {
      id: '37',
      name: 'Goal.com',
      avatarUrl: '/assets/users/goal.png'
    }, {
      id: '38',
      name: 'JWOWW',
      avatarUrl: '/assets/users/jwoww.png'
    }, {
      id: '39',
      name: 'Lowe\'s Home Improvement',
      avatarUrl: '/assets/users/lowes.png'
    }, {
      id: '40',
      name: 'TechCrunch',
      avatarUrl: '/assets/users/tc.png'
    }, {
      id: '41',
      name: 'ABC News',
      avatarUrl: '/assets/users/abc.png'
    }, {
      id: '42',
      name: 'Adidas',
      avatarUrl: '/assets/users/adidas.png'
    }, {
      id: '43',
      name: 'British Airways',
      avatarUrl: '/assets/users/britishairways.png'
    }, {
      id: '44',
      name: 'BuzzFeed',
      avatarUrl: '/assets/users/buzz.png'
    }, {
      id: '45',
      name: 'CNBC',
      avatarUrl: '/assets/users/cnbc.png'
    }, {
      id: '46',
      name: 'Complex',
      avatarUrl: '/assets/users/complex.png'
    }, {
      id: '47',
      name: 'Goal.com',
      avatarUrl: '/assets/users/goal.png'
    }, {
      id: '48',
      name: 'JWOWW',
      avatarUrl: '/assets/users/jwoww.png'
    }, {
      id: '49',
      name: 'Lowe\'s Home Improvement',
      avatarUrl: '/assets/users/lowes.png'
    }, {
      id: '50',
      name: 'TechCrunch',
      avatarUrl: '/assets/users/tc.png'
    }];

    return {users};
  }
}
