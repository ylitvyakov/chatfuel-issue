import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { UsersComponent } from './components/user/users/users.component';
import { UserComponent } from './components/user/user/user.component';

const routes: Routes = [{
  path: '',
  redirectTo: 'users',
  pathMatch: 'full'
}, {
  path: 'users',
  component: UsersComponent
}, {
  path: 'users/:id',
  component: UserComponent
}];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
